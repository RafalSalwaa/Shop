<?php

declare(strict_types=1);

namespace App\Exception;

use Exception;

final class InvalidCouponCodeException extends Exception
{
}
