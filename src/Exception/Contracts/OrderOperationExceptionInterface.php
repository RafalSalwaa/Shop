<?php

declare(strict_types=1);

namespace App\Exception\Contracts;

use Throwable;

interface OrderOperationExceptionInterface extends Throwable
{
}
